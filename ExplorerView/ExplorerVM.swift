//
//  ExplorerVM.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import Foundation

class ExplorerVM: ObservableObject {
    var trendingMovies: [Video] {
        RandomModelGenerator.generateVideos(number: 15)
    }
}

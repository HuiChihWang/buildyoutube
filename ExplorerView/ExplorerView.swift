//
//  ExplorerView.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import SwiftUI

struct ExplorerView: View {
    @ObservedObject var vm = ExplorerVM()
    
    var body: some View {
        ScrollView {
            VideoStacks(videos: vm.trendingMovies)
        }
    }
}

struct ExplorerView_Previews: PreviewProvider {
    static var previews: some View {
        ExplorerView()
    }
}

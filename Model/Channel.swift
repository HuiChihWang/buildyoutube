//
//  Channel.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/26.
//

import Foundation

struct Channel {
    var creator: Creator
    var playList: [PlayList]
}

//
//  Video.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/24.
//

import Foundation

struct Video: Identifiable {
    var videoURL: URL
    
    var title: String
    var viewNumber: Int
    var publishedTime: Int
    var creator: Creator
    
    var id = UUID()
}

//
//  File.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/24.
//

import Foundation

struct Creator: Identifiable {
    var name: String
    var photoURL: URL
    var id = UUID()
}

//
//  SubscriptionBar.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/26.
//

import SwiftUI

struct SubscriptionBar: View {
    var creators: [Creator]
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack {
                ForEach(creators) {creator in
                    VStack {
                        ProfilePictureView(imageURL: creator.photoURL)
                        Text(creator.name)
                            .font(.subheadline)
                            .foregroundColor(.gray)
                    }
                    .frame(width: 90, height: 90)
                    .padding(.horizontal, 10)

                }
            }
        }
    }
}

struct SubscriptionBar_Previews: PreviewProvider {
    static var previews: some View {
        SubscriptionBar(creators: RandomModelGenerator.generateCreators(number: 10))
    }
}

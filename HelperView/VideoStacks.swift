//
//  VideoStacks.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/26.
//

import SwiftUI

struct VideoStacks: View {
    var videos: [Video]
    var body: some View {
        LazyVStack {
            ForEach(videos) { movie in
                VideoView(video: movie)
                    .padding(.bottom, 10)
            }
        }
    }
}

struct VideoStacks_Previews: PreviewProvider {
    static var previews: some View {
        VideoStacks(videos: RandomModelGenerator.generateVideos(number: 8))
    }
}

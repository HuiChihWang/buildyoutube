//
//  TagButton.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/26.
//

import SwiftUI

struct TagButton: View {
    var text: String
    var action: () -> Void = {}

    @State var isActive = false
    
    private static let cornerRadius: CGFloat = 17
    
    var body: some View {
        Button(action: {
            isActive = true
            self.action()
        }, label: {
            Text(text)
                .font(.system(size: 20))
                .padding(.horizontal, 10)
                .padding(.vertical, 7)
                .foregroundColor(isActive ? .white : .black)
                .background(isActive ? Color(#colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)) : Color(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)))
                .cornerRadius(TagButton.cornerRadius)
                .overlay(
                    RoundedRectangle(cornerRadius: TagButton.cornerRadius)
                        .stroke(lineWidth: 0.3)
                        .foregroundColor(.black)
                )
        })
    }
}

struct TagButton_Previews: PreviewProvider {
    static var previews: some View {
        TagButton(text: "Hello")
    }
}

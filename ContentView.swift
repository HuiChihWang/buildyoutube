//
//  ContentView.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/24.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        
        VStack {
            TopBarView()
                .frame(height: 30)
                .padding(.top, 10)
            
            TabView {
                HomeView()
                    .tabItem {
                        Image(systemName: "house")
                        Text("Home")
                    }

                ExplorerView()
                    .tabItem {
                        Image(systemName: "safari")
                        Text("Explore")
                    }

                SubscriptionView()
                    .tabItem {
                        Image(systemName: "icloud.fill")
                        Text("Subscriptions")
                    }
                
                LibraryView()
                    .tabItem {
                        Image(systemName: "play.rectangle")
                        Text("Library")
                    }
            }
            .accentColor(.black)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

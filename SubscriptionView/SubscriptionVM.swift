//
//  SubscriptionVM.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/26.
//

import Foundation

class SubscriptionVM: ObservableObject {
    
    var subscribedChannel: [Creator] {
        RandomModelGenerator.generateCreators(number: 10)
    }
    
    var tags: [Category] {
        Category.allCases
    }
    
    public func getCatogoriesMovie(for category: Category) -> [Video] {
        RandomModelGenerator.generateVideos(number: 10)
    }
}

enum Category: String, CaseIterable {
    case All
    case BasketBall
    case BaseBall
    case News
    case Politics
}


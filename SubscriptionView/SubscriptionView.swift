//
//  SubscriptionView.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import SwiftUI

struct SubscriptionView: View {
    @ObservedObject var vm = SubscriptionVM()
    @State private var chooseCategory: Category = .All
    
    var body: some View {
        ScrollView {
            VStack(spacing: 0) {
                SubscriptionBar(creators: vm.subscribedChannel)
                    .frame(height: 130)
                
                LineView(length: screenSize.width)
                
                TagsView(catogories: vm.tags, chooseCategory: $chooseCategory)
                    .frame(height: 40)
                    .padding(.vertical, 10)
                    .padding(.leading, 10)
                
                VideoStacks(videos: vm.getCatogoriesMovie(for: chooseCategory))
        
            }
        }
        
    }
}

struct SubscriptionView_Previews: PreviewProvider {
    static var previews: some View {
        SubscriptionView()
    }
}

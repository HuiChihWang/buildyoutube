//
//  VideoCellView.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import SwiftUI
import KingfisherSwiftUI

struct VideoCellView: View {
    var video: Video
    var body: some View {
        GeometryReader { geo in
            VStack(alignment: .leading) {
                // Video View
                KFImage(video.videoURL)
                    .resizable()
                    .scaledToFill()
                    .frame(width: geo.size.width, height: geo.size.width / 1.3)
                    .clipped()
                
                VStack(alignment: .leading) {
                    Text(video.title)
                        .font(.system(size: 20))
                        .lineLimit(2)
                    
                    Text(video.creator.name)
                        .font(.subheadline)
                        .foregroundColor(.gray)
                }
                
            }
        }
    }
}

struct VideoCellView_Previews: PreviewProvider {
    static var previews: some View {
        VideoCellView(video: exampleVideo1)
            .frame(width: 200)
    }
}

//
//  VideoScrollerView.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import SwiftUI

struct VideoScrollerView: View {
    var videos: [Video]
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack {
                ForEach(videos) { video in
                    VideoCellView(video: video)
                        .frame(width: screenSize.width / 2.5, height:190)
                }
            }
        }
    }
}

struct VideoScrollerView_Previews: PreviewProvider {
    static var previews: some View {
        let exampleVideos = RandomModelGenerator.generateVideos(number: 10)
        return VideoScrollerView(videos: exampleVideos)
    }
}

//
//  LibraryView.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import SwiftUI

struct LibraryView: View {
    @ObservedObject var vm = LibraryVM()
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Text("Recent")
                    .font(.title2)
                VideoScrollerView(videos: vm.recentVideos)
                    .frame(height: 200)
            }
            
            LineView(length: screenSize.width)
                .padding(.vertical, 10)
            
            LibraryMenuView()
                .frame(height: screenSize.height * 0.2)
            
            
            LineView(length: screenSize.width)
                .padding(.vertical, 10)

            
            AllPlayListView(playLists: vm.playLists)
            
        }
        .padding(.top, 10)

    }
}

struct LibraryView_Previews: PreviewProvider {
    static var previews: some View {
        LibraryView()
    }
}

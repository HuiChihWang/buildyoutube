//
//  PlayList.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import Foundation

struct PlayList: Identifiable {
    var name: String
    var videos: [Video]
    
    var numberOfVideos: Int {
        return videos.count
    }
    
    var alblumImageURL: URL {
        exampleImageURL
    }
    
    var id = UUID()
}

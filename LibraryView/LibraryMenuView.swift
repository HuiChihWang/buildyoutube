//
//  LibraryMenuView.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import SwiftUI

struct LibraryMenuView: View {
    var body: some View {
            VStack {
                HorizontalButton(imageName: "clock.arrow.circlepath", text: "History") {
                    
                }
                Spacer()
                
                HorizontalButton(imageName: "play.rectangle", text: "My videos") {
                    
                }
                Spacer()
                
                HorizontalButton(imageName: "film.fill", text: "Your movies") {
                    
                }
                Spacer()
                
                HorizontalButton(imageName: "clock", text: "Watch later") {
                    
                }
            }
            .foregroundColor(.black)
    }
}

struct HorizontalButton: View {
    var imageName: String
    var text: String
    var action: () -> Void
    
    var body: some View {
        Button(action: action, label: {
            HStack {
                Image(systemName: imageName)
                    .font(.system(size: 30))
                    .frame(width: 50)
                    .padding(.trailing, 20)
                
                Text(text)
                    .font(.title2)
                Spacer()
            }
            
        })
    }
}

struct LibraryMenuView_Previews: PreviewProvider {
    static var previews: some View {
        LibraryMenuView()
            .frame(height: 200)
    }
}

//
//  LineView.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/26.
//

import SwiftUI

struct LineView: View {
    var length: CGFloat
    var color = Color.gray
    var lineWidth: CGFloat = 0.5
    
    var body: some View {
        Rectangle()
            .foregroundColor(color)
            .frame(width: length, height: lineWidth)
    }
}

struct LineView_Previews: PreviewProvider {
    static var previews: some View {
        LineView(length: 100)
    }
}

//
//  PlayListView.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import SwiftUI
import KingfisherSwiftUI

struct AllPlayListView: View {
    var playLists: [PlayList]
    
    var body: some View {
        
        VStack (alignment: .leading){
            HStack {
                Text("Playlists")
                    .font(.title2)
                Spacer()
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                    Text("Recently Added")
                        .font(.system(size: 15, weight: .bold))
            
                    
                    Image(systemName: "chevron.down")
                })
                .foregroundColor(.black)
                .padding(.trailing, 5)
            }
            
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                HStack {
                    Image(systemName: "plus")
                        .font(.system(size: 30))
                    
                    Text("New Play List")
                        .font(.title2)
                        .padding(.leading, 10)
                }
            })
            .foregroundColor(.blue)
            .padding(.vertical, 20)
            
            VStack {
                ForEach(playLists) { playList in
                    PlayListView(playList: playList)
                        .frame(height: 50)
                }
            }

        }
    }
}

struct PlayListView: View {
    var playList: PlayList
    
    var body: some View {
        GeometryReader { geo in
            HStack {
                KFImage(playList.alblumImageURL)
                    .resizable()
                    .scaledToFill()
                    .frame(width: geo.size.height * 0.9, height: geo.size.height * 0.9)
                    .clipped()
                
                VStack(alignment: .leading) {
                    Text(playList.name)
                        .font(.system(size: 15))
                    Text("\(playList.numberOfVideos) videos")
                        .font(.subheadline)
                        .foregroundColor(.gray)
                }
                .padding(.leading, 10)
            }
        }
    }
}



struct PlayListView_Previews: PreviewProvider {
    static var previews: some View {
        AllPlayListView(playLists: examplePlayLists)
    }
}

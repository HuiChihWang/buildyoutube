//
//  LibraryVM.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import Foundation


class LibraryVM: ObservableObject {
    
    
    var recentVideos: [Video] {
        RandomModelGenerator.generateVideos(number: 10)
    }
    
    var playLists: [PlayList] {
        examplePlayLists
    }
}

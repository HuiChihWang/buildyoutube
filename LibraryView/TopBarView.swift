//
//  TopBarView.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import SwiftUI

struct TopBarView: View {
    
    var body: some View {
        HStack {
            Image("Youtube")
                .resizable()
                .scaledToFit()
            Spacer()
            
            Image(systemName: "bell")
                .resizable()
                .scaledToFit()
            
            Image(systemName: "magnifyingglass")
                .resizable()
                .scaledToFit()
                .padding(.horizontal, 20)
            
            // TODO: user info
            Color.black
                .clipShape(Circle())
                .frame(width: 30)

        }
    }
}

struct TopBarView_Previews: PreviewProvider {
    static var previews: some View {
        TopBarView()
            .frame(height: 30)
    }
}

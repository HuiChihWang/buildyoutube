//
//  BuildYoutubeApp.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/24.
//

import SwiftUI

@main
struct BuildYoutubeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

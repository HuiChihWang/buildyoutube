//
//  VideoView.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/24.
//

import SwiftUI
import KingfisherSwiftUI

struct VideoView: View {
    var video: Video
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            Color.black
                .overlay(
                    Text("Video")
                        .foregroundColor(.white)
                        .font(.title)
                )
                .frame(height: 250)
            
            
            HStack {
                KFImage(video.creator.photoURL)
                    .resizable()
                    .aspectRatio(1, contentMode: .fit)
                    .clipShape(Circle())
                    .frame(height: 50)
                
                VStack(alignment: .leading) {
                    Text(video.title)
                    
                    HStack {
                        Text(video.creator.name)
                        Image(systemName: "circle.fill")
                            .font(.system(size: 3))
                        Text("\(video.viewNumber) views")
                        Image(systemName: "circle.fill")
                            .font(.system(size: 3))
                        Text("\(video.publishedTime) minutes ago")
                        
                    }
                    .font(.subheadline)
                    .foregroundColor(.gray)
                }
                .padding(.leading, 5)
                .padding(.top, 10)
            }
            .padding(.leading, 10)
        }
    }
}

struct VideoView_Previews: PreviewProvider {
    static var previews: some View {
        VideoView(video: exampleVideo1)
            .frame(height:400)
    }
}

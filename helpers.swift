//
//  helpers.swift
//  BuildYoutube
//
//  Created by Hui Chih Wang on 2020/11/24.
//

import Foundation
import SwiftUI


let screenSize = UIScreen.main.bounds

var exampleImageURL: URL {
    URL(string: "https://picsum.photos/400/300")!
}

let exampleCreator = Creator(name: "Gilbert", photoURL: exampleImageURL)

let exampleVideo1 = Video(
    videoURL: exampleImageURL,
    title: "Is it worth learning iOS development in 2020 ?",
    viewNumber: 6666,
    publishedTime: 43,
    creator: exampleCreator)

class RandomModelGenerator {    
    static let creator = Creator(name: "Gilbert", photoURL: exampleImageURL)
    
    static func videoGenerator() -> Video {
        Video(videoURL: exampleImageURL, title: "Is it worth learning iOS development in 2020 ?", viewNumber: 1280, publishedTime: 43, creator: creator)
    }
    
    static func generateVideos(number: Int) -> [Video] {
        (0..<number).map{ _ in videoGenerator()}
    }
    
    static func generateCreator() -> Creator {
        Creator(name: "Gilbert", photoURL: exampleImageURL)
    }
    
    static func generateCreators(number: Int) -> [Creator] {
        (0..<number).map { _ in generateCreator()}
    }
}

let examplePlayList1 = PlayList(name: "EDM", videos: RandomModelGenerator.generateVideos(number: 4))
let examplePlayList2 = PlayList(name: "Chinese", videos: RandomModelGenerator.generateVideos(number: 2))
let examplePlayList3 = PlayList(name: "English", videos: RandomModelGenerator.generateVideos(number: 5))
let examplePlayList4 = PlayList(name: "Comedy", videos: RandomModelGenerator.generateVideos(number: 7))

let examplePlayLists = [examplePlayList1, examplePlayList2, examplePlayList3, examplePlayList4]
